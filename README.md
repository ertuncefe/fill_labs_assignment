# fill_labs_assignment

This is a small assignment for the technical evaluation part of the job application.

***

# Fill - Labs Assignment Progress

Before starting the task related explanations, first I wanted to mention that the learning phase of the new concepts inside the task which is sent to me by Fill - Labs team.

In this section, I will explain about my researches and my learning methods about the go programming language which is quite a new challenge for me. Since I do not have any previous experience in golang, I've needed to learn the basics of golang in order to make myself comfortable while implementing the assignments.

To learn basics of golang, I've looked at the go documentation from golang.org, and also I've watched some youtube video tutorial series;
 - [Golang crash course](https://www.youtube.com/watch?v=SqrbIlUwR0U)
 - [Golang REST API with mux](https://www.youtube.com/watch?v=SonwZ6MF5BE)
 - [An Introduction to Testing in Go](https://www.youtube.com/watch?v=GlA57dHa5Rg&t=2s)

Other than that, I've found some other useful resources when I am stuck while implementing the code;

- [The Go Programming Language Specification](https://golang.org/ref/spec)
- [The 3 ways to sort in Go](https://yourbasic.org/golang/how-to-sort-in-go/)
- [What is the Big O performance of maps in golang?](https://stackoverflow.com/questions/29677670/what-is-the-big-o-performance-of-maps-in-golang)
- [Reading in Console Input in Golang](https://tutorialedge.net/golang/reading-console-input-golang/)
- [How to sort a slice stable in Golang](https://www.geeksforgeeks.org/how-to-sort-a-slice-stable-in-golang/)
- [How To Write Unit Tests in Go](https://www.digitalocean.com/community/tutorials/how-to-write-unit-tests-in-go-using-go-test-and-the-testing-package)
- [Creating a RESTful API With Golang](https://tutorialedge.net/golang/creating-restful-api-with-golang/)
- [Understanding go.sum and go.mod file in Go](https://golangbyexample.com/go-mod-sum-module/)
- [The ternary operator in GoLang](https://golangdocs.com/ternary-operator-in-golang)
- [Unit Testing for REST APIs in Go](https://codeburst.io/unit-testing-for-rest-apis-in-go-86c70dada52d)
- [Buffer string() not equal string](https://stackoverflow.com/questions/45275185/buffer-string-not-equal-string)
- [How to set URL parameters for unit tests?](https://github.com/gorilla/mux/issues/167)
- [Golang ORM Tutorial](https://tutorialedge.net/golang/golang-orm-tutorial/)
- [Golang MySQL Tutorial](https://tutorialedge.net/golang/golang-mysql-tutorial/) 
- [REST-API with Golang, Mux & MySQL](https://hugo-johnsson.medium.com/rest-api-with-golang-mux-mysql-c5915347fa5b)
- [Go using mux Router - How to pass my DB to my handlers](https://stackoverflow.com/questions/33646948/go-using-mux-router-how-to-pass-my-db-to-my-handlers)
- [DATE IN GOLANG AND MYSQL DATABASE](https://learningprogramming.net/golang/golang-and-mysql/date-in-golang-and-mysql-database/)
- [Executing transactions](https://golang.org/doc/database/execute-transactions)

## Progress plan of the assignment process
1. Create the development environment on your local
2. Create a gitlab repository and test it with simple hello world application.
3. Create the README file with simple explanations and future planning of the assignment step by step.
4. Read the assignment, plan and explain the design & analysis, implementation, testing and deployment (for 4th question) phases of the whole assignment.
5. Make necessary changes on README.
6. Send the assignment.

***

## Design & Analysis Phase 
In this section, design and analysis processes of the questions will be explained briefly;

### question_01
This problem can be solved by implementing following steps;

1. Get the input slice and create a slice with the specially prepared object which includes count of 'a' chars inside the word, lenght of the word and the word itself. Creating a map will take a O(n*m) as a time complexity where n is the lenght of the slice (looping all the tokens) and m is the lenght of the word strings (getting the count of "a"s inside the string takes O(m)).
2. Sort this map according to their lenghts first and then sort again according to their "a" count both sortings should be decreasing order and also should be stable which means the equal ones according to the less function will not be randomly distributed. This sorting will take a O(nlogn) if we use the quick sort algorihtm on average case. Also in Go sort package make O(nlogn) comparisons in worst case [according to this source](https://yourbasic.org/golang/how-to-sort-in-go/). So, we can use the integer sorting algorithm while implementing our solution.
3. Prepare the output and print the desired result as an output. Also this will take the O(n) time complexity.

As a result we can say that the total time complexity of the created algorithm will be the O(n*m) according to the 1st step which is the bottleneck in this solution. Note that, if logn is greater than the m, time complexity will be the O(nlogn) in average. Thus we can say that larger count of tokens will take O(nlogn) time.

### question_02

First of all, there is a need to find the right algorithm to produce that output logically. The produced output for the input n can be found by sqrt(n)(square root of the n) then print sqrt(n)-1 , sqrt(n)+1, and n respectively. For instance for the input 9 the result will be 2 4 9. This algorithm can be implemented recursively while using Newton's square root method [[1]](https://en.wikipedia.org/wiki/Newton%27s_method). Other operations will be take constant time which is O(1) but this sqrt operation will take O((log n) F(n)) where F(n) is the cost of calculating f(x)/f'(x)\, with n-digit precision [[2]](https://en.citizendium.org/wiki/Newton's_method#Computational_complexity). 

### question_03

We can solve this question while writing by two nested for loop and we can take the word which has biggest occurence count at the end but this takes O(n^2) time.

Instead of above solution we can produce a map and hold all of the words' occurence counts which are in the given input array in a just one for loop. This obviously creates a space complexity but, from the time complexity perspective it will be O(n).

Also in the [go language specs](https://golang.org/ref/spec#Map_types) there is no performance guarantee for hashmaps but in general, hashmaps has O(1) complexity for looking up, inserting, and deleting in average [[3]](https://www.tutorialspoint.com/Difference-between-TreeMap-HashMap-and-LinkedHashMap-in-Java#:~:text=HashMap%20has%20complexity%20of%20O,does%20not%20maintain%20any%20order.). As a result we can say that the time complexity of this solution will be O(n) in average. 

### question_04

The solution for this question, I will be implemented the simple react application and a REST API which is implemented with go that connects the MySQL DB instance for persistance. 

In general, for backend applications using an ORM (Object-Relational Mapping) library is more convenient way. In that way ORM tool that we are using creates a bridge with us (as a developer) and the database that we are using. More simply, we do not need to create simple SQL queries and if we need to write a complex one, we can still write the needed SQL script inside our codebase. 

Also it can be used some migration libraries to make the backend application database environment independent. (such as liquibase on spring boot ) 

But for this simple application, I will create a one database entity which will be the "user" table. Because of that, I think a MySQL database will be enough for my solution

For the frontend applications, designed mockups can be found below;

[Master View Mockup](https://gitlab.com/ertuncefe/fill_labs_assignment/-/raw/main/mockups/Master%20view%20mockup.jpg)

[Detailed View Mockup - create](https://gitlab.com/ertuncefe/fill_labs_assignment/-/raw/main/mockups/Detailed%20view%20-%20create%20mockup.jpg)

[Detailed View Mockup - update](https://gitlab.com/ertuncefe/fill_labs_assignment/-/raw/main/mockups/Detailed%20view%20-%20update%20mockup.jpg)

[Detailed View Mockup - delete](https://gitlab.com/ertuncefe/fill_labs_assignment/-/raw/main/mockups/Detailed%20view%20-%20delete%20mockup.jpg)

***
## Deployment Phase for 4th Question
Deployment of this RESTful API and also the react application can be done with building docker images, and running these image on the containers. 

***
# Other Necessary Explanations About the Assignment [Documentation Part of the applications]

## question_01
To run the solution, in the question_01 directory "go run main.go" command can be run.
To run the unit test, "go test" command can be run in the same directory.
To build and create the executable "go build" command will be enough, the executable will be created with the name question_01 which can be run by typing "./question_01".  
## question_02
To run the solution, in the question_02 directory "go run main.go" command can be run.
To run the unit test, "go test" command can be run in the same directory.
To build and create the executable "go build" command will be enough, the executable will be created with the name question_02 which can be run by typing "./question_02".
## question_03
To run the solution, in the question_03 directory "go run main.go" command can be run.
To run the unit test, "go test" command can be run in the same directory.
To build and create the executable "go build" command will be enough, the executable will be created with the name question_03 which can be run by typing "./question_03".
## question_04
To run the solution, in the question_04 directory "go run main.go" command can be run.
To run the unit test, "go test -v" command can be run in the same directory. With the -v argument, all the tests will be run.
To build and create the executable "go build" command will be enough, the executable will be created with the name question_04 which can be run by typing "./question_04".

### API Documentation Table

#### Base Api URL on local: http://localhost:8099

| HTTP METHOD   | URL           | SAMPLE BODY   |
| ------------- |:-------------:| -------------:|
| GET           | /getAll       |       -       |
| GET           | /get/{id}     |       -       |
| POST          | /create       |{"name":"Fahri","surname":"Görünmez","profession":"Architect","birthdate":"1988-03-03","gender":"male","profilepicture": "base64 encoded image"}|
| PUT           | /update/{id}  |{"name":"Osman Ural","surname":"KESER","profession":"Computer Science","birthdate":"1993-05-04","gender":"male","profilepicture":"base64 encoded image"}|
| DELETE        | /delete/{id}  |       -       |

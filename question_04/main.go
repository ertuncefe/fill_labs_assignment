package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type User struct {
	ID             int    `json:"id"`
	Name           string `json:"name"`
	Surname        string `json:"surname"`
	Profession     string `json:"profession"`
	Birthdate      string `json:"birthdate"`
	Gender         string `json:"gender"`
	ProfilePicture string `json:"profilepicture"`
}

//DB Config
const DB_TYPE string = "mysql"
const DB_USER string = "root"
const DB_USER_PASSWORD string = "rootroot"
const DB_HOST string = "127.0.0.1"
const DB_PORT string = "3306"
const DB_DEFAULT_SCHEMA string = "fill_labs_db"

//Global DB object
var db *sql.DB

// Error Types
const USER_FETCH_ERROR string = "User not found!"
const USERS_NOT_FOUND string = "There is no user in the system database!"
const INVALID_USER_CREATION_REQUEST string = "User create request is not in the right format!"
const USER_DELETED_SUCCESSFULLY string = "User deleted successfully!"
const INVALID_USER_UPDATE_REQUEST string = "User update request is not in the right format!"
const USER_UPDATED_SUCCESSFULLY string = "User updated successfully!"
const USER_CANNOT_BE_DELETED string = "User deletion unsuccesfull!"

func restAPIPage(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writer, "Fill-Labs Simple User Management RESTful API")
}

func returnAllUsers(writer http.ResponseWriter, request *http.Request) {
	//create user slice for getting data into main memory from DB
	var allUsers []User

	//Prepare the content type on header
	writer.Header().Set("Content-Type", "application/json")

	//Get the all users from DB
	result, err := db.Query("SELECT usr.id, usr.name, usr.surname, usr.profession," +
		" usr.birthdate, usr.gender, usr.profile_picture FROM user usr")
	//Handle the error if occurs
	if err != nil {
		panic(err.Error())
	}
	// Close the transactional connection
	defer result.Close()

	for result.Next() {
		var user User
		//Scan the returned result from db and deserialize as user object
		err := result.Scan(
			&user.ID,
			&user.Name,
			&user.Surname,
			&user.Profession,
			&user.Birthdate,
			&user.Gender,
			&user.ProfilePicture)
		if err != nil {
			panic(err.Error())
		}
		allUsers = append(allUsers, user)
	}
	//Encode all the users gathered DB, if there is no user, return not found
	if allUsers == nil {
		json.NewEncoder(writer).Encode(USERS_NOT_FOUND)
	} else {
		json.NewEncoder(writer).Encode(allUsers)
	}
}

func returnSingleUser(writer http.ResponseWriter, request *http.Request) {
	//Prepare the content type on header
	writer.Header().Set("Content-Type", "application/json")

	//Get the given id parameter from the request
	paramUserID := mux.Vars(request)["id"]

	var foundUser User
	//Find the correspondent user from DB and encode it into response
	result, err := db.Query("SELECT usr.id, usr.name, usr.surname, usr.profession,"+
		" usr.birthdate, usr.gender, usr.profile_picture FROM user usr WHERE usr.id = ?", paramUserID)
	//Handle error if occurs
	if err != nil {
		panic(err.Error())
	}
	//close the transaction
	defer result.Close()
	//Scan the returned result from DB and get the user
	for result.Next() {
		err := result.Scan(
			&foundUser.ID,
			&foundUser.Name,
			&foundUser.Surname,
			&foundUser.Profession,
			&foundUser.Birthdate,
			&foundUser.Gender,
			&foundUser.ProfilePicture)
		if err != nil {
			panic(err.Error())
		}
	}

	if (foundUser != User{}) {
		json.NewEncoder(writer).Encode(foundUser)
	} else {
		json.NewEncoder(writer).Encode(USER_FETCH_ERROR)
	}
}

func createNewUser(writer http.ResponseWriter, request *http.Request) {
	//Prepare the content type on response header
	writer.Header().Set("Content-Type", "application/json")

	//Get the http body of the request
	requestBody, err := ioutil.ReadAll(request.Body)

	if err != nil {
		json.NewEncoder(writer).Encode(INVALID_USER_CREATION_REQUEST)
	}

	//Unmarshal the body into a new user object
	var newUser User
	json.Unmarshal(requestBody, &newUser)
	parsedBirthdate, _ := time.Parse("2006-01-02", newUser.Birthdate)

	//Persist the newlyCreated user object into DB
	statement, err := db.Prepare("INSERT INTO user(name, surname, profession, birthdate, gender, profile_picture) VALUES(?, ?, ?, ?, ?, ?)")
	if err != nil {
		panic(err.Error())
	}
	//Execute the insert SQL
	result, err := statement.Exec(
		newUser.Name,
		newUser.Surname,
		newUser.Profession,
		parsedBirthdate,
		newUser.Gender,
		newUser.ProfilePicture)

	// Handle the execution if error occures in DB transaction
	if err != nil {
		panic(err.Error())
	}

	//Send the response
	newUsrId, _ := result.LastInsertId()
	newUser.ID = int(newUsrId)
	json.NewEncoder(writer).Encode(newUser)
}

func updateUser(writer http.ResponseWriter, request *http.Request) {
	//Prepare the content type on response header
	writer.Header().Set("Content-Type", "application/json")

	//Get the given id parameter from the request
	paramUserID := mux.Vars(request)["id"]

	//Get the http body of the request
	requestBody, err := ioutil.ReadAll(request.Body)

	if err != nil {
		json.NewEncoder(writer).Encode(INVALID_USER_UPDATE_REQUEST)
	}
	//Unmarshal the body into a new user object which contains the updated user information
	var updatedUserInfo User
	json.Unmarshal(requestBody, &updatedUserInfo)
	parsedBirthdate, _ := time.Parse("2006-01-02", updatedUserInfo.Birthdate)
	//Prepare sql statement
	statement, err := db.Prepare("UPDATE user usr SET usr.name = ?, usr.surname = ?, usr.profession = ?," +
		" usr.birthdate = ?, usr.gender = ?, usr.profile_picture = ? WHERE usr.id = ?")
	if err != nil {
		panic(err.Error())
	}

	_, err = statement.Exec(
		updatedUserInfo.Name,
		updatedUserInfo.Surname,
		updatedUserInfo.Profession,
		parsedBirthdate,
		updatedUserInfo.Gender,
		updatedUserInfo.ProfilePicture,
		paramUserID)
	if err != nil {
		panic(err.Error())
	}

	//prepare response message
	json.NewEncoder(writer).Encode(USER_UPDATED_SUCCESSFULLY)
}

func deleteUser(writer http.ResponseWriter, request *http.Request) {
	//Prepare the content type on response header
	writer.Header().Set("Content-Type", "application/json")

	//Get the given id parameter from the request
	paramUserID := mux.Vars(request)["id"]

	statement, err := db.Prepare("DELETE FROM user usr WHERE usr.id = ?")
	if err != nil {
		panic(err.Error())
	}
	result, err := statement.Exec(paramUserID)
	if err != nil {
		panic(err.Error())
	}

	//Prepare the response
	deleted, _ := result.RowsAffected()
	if deleted != 0 {
		json.NewEncoder(writer).Encode(USER_DELETED_SUCCESSFULLY)
	} else {
		json.NewEncoder(writer).Encode(USER_CANNOT_BE_DELETED)
	}

}

func handleRequests() {
	router := mux.NewRouter()

	router.HandleFunc("/", restAPIPage).Methods("GET")
	router.HandleFunc("/getAll", returnAllUsers).Methods("GET")
	router.HandleFunc("/get/{id}", returnSingleUser).Methods("GET")
	router.HandleFunc("/create", createNewUser).Methods("POST")
	router.HandleFunc("/update/{id}", updateUser).Methods("PUT")
	router.HandleFunc("/delete/{id}", deleteUser).Methods("DELETE")

	fmt.Println("Server up and listening...")
	log.Fatal(http.ListenAndServe(":8099", router))
}

func main() {
	//Prepare the DB object for future use
	var err error
	db, err = sql.Open(DB_TYPE, DB_USER+":"+DB_USER_PASSWORD+"@tcp("+DB_HOST+":"+DB_PORT+")/"+DB_DEFAULT_SCHEMA)

	// if an error occurs, handle it
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	fmt.Println("DB object created successfully...")

	handleRequests()

}

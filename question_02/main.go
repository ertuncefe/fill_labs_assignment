package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

//Define constant tolerance level
const TOLERANCE float64 = 0.01

// Implement the Newton's square root function recursively
func NewtonSqrt(guess float64, input float64) float64 {

	//if square of the guessed number minus the input value is nearly equal then return the guessed number as square root
	if math.Abs((input - (guess * guess))) <= TOLERANCE {
		return guess
	} else {
		// guess the new possible square root value and call the NewtonSqrt function again
		guessSqrtAgain := (guess*guess + input) / float64(2*guess)
		return NewtonSqrt(guessSqrtAgain, input)
	}
}

func main() {

	// Get the input from the console using bufio scanner and convert it to integer
	fmt.Println("Please enter an input: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input, _ := strconv.Atoi(scanner.Text())

	//Call the newtons square root function that written recursively
	squareRoot := NewtonSqrt(float64(input)/2, float64(input))

	//Print the desired result
	fmt.Println(int(squareRoot) - 1)
	fmt.Println(int(squareRoot) + 1)
	fmt.Println(input)
}

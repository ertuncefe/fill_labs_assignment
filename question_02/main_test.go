package main

import (
	"strconv"
	"testing"
)

func TestTableNewtonSqrt(t *testing.T) {
	var testSlice = []struct {
		input    int
		expected int
	}{
		//Test 1
		{4, 2},
		//Test 2
		{9, 3},
		//Test 3
		{16, 4},
		//Test 4
		{625, 25},
		//Test 5
		{144, 12},
		//Test 6
		{25, 5},
	}

	for _, test := range testSlice {
		testResult := NewtonSqrt(float64(test.input)/2, float64(test.input))
		if int(testResult) != test.expected {
			t.Error("Test Failed! Output is " + strconv.Itoa(int(testResult)) + " but expected " + strconv.Itoa(test.expected))
		}
	}
}

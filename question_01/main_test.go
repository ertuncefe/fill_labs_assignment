package main

import "testing"

func TestTableSpecialSort(t *testing.T) {
	var testSlice = []struct {
		input    string
		expected string
	}{
		//Test 1
		{"[\"aaaasd\", \"a\", \"aab\", \"aaabcd\", \"ef\", \"cssssssd\", \"fdz\", \"kf\", \"zc\", \"lklklklklklklklkl\", \"l\"]",
			"[\"aaaasd\", \"aaabcd\", \"aab\", \"a\", \"lklklklklklklklkl\", \"cssssssd\", \"fdz\", \"ef\", \"kf\", \"zc\", \"l\"]"},
		//Test 2
		{"[\"aaaa\", \"aaaaty\", \"aaab\", \"aaabcd\", \"ef\", \"cssssssd\"]",
			"[\"aaaaty\", \"aaaa\", \"aaabcd\", \"aaab\", \"cssssssd\", \"ef\"]"},
		//Test 3
		{"[\"a\", \"aa\", \"aaa\", \"aaab\", \"aaaaa\", \"aaaaaaaaaa\"]",
			"[\"aaaaaaaaaa\", \"aaaaa\", \"aaab\", \"aaa\", \"aa\", \"a\"]"},
	}

	for _, test := range testSlice {
		testResult := SpecialSort(test.input)
		if testResult != test.expected {
			t.Error("Test Failed! Output is " + testResult + " but expected " + test.expected)
		}
	}
}

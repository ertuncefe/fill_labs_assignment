package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
)

// Model of a hashmap object
type hashMapObj struct {
	Word      string
	Lenght    int
	CharCount int
}

func SpecialSort(inputStr string) string {
	//Tokenize that input into a slice get rid of the quotes, split it with delimiter
	//", " and get rid of the braces
	filteredStr := strings.Replace(inputStr, "\"", "", -1)
	tokenizedSlice := strings.Split(filteredStr[1:len(filteredStr)-1], ", ")

	//Get the words one by one from the slice and create a slice with the hashMap struct
	//which includes the count of "a" chars, lenghts of the each word, and the word itself
	var wordMap []hashMapObj
	for _, word := range tokenizedSlice {
		wordMap = append(wordMap, hashMapObj{word, len(word), strings.Count(word, "a")})
	}

	//Sort the wordMap according to their lenght in decreasing order. In that way,
	//the words that contains same amount of charater "a"s will be sorted by their length
	//when we will stable sort them according to their character "a"s count later.
	sort.SliceStable(wordMap, func(i, j int) bool {
		return wordMap[i].Lenght > wordMap[j].Lenght
	})

	//Sort the wordMap according to their a character count in decreasing order, while using sort stable.
	sort.SliceStable(wordMap, func(i, j int) bool {
		return wordMap[i].CharCount > wordMap[j].CharCount
	})

	//Prepare desired output and print it to the console
	LAST_INDEX := len(wordMap) - 1
	var resultToBePrinted string = "["

	for index, mapObject := range wordMap {

		if index == LAST_INDEX {
			resultToBePrinted += "\"" + mapObject.Word + "\"]"
		} else {
			resultToBePrinted += "\"" + mapObject.Word + "\", "
		}

	}

	return resultToBePrinted
}

func main() {

	// Get the input from the console using bufio scanner
	fmt.Println("Please enter an input as a string slice: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	inputStr := scanner.Text()

	//Sort and print the desired result
	fmt.Println(SpecialSort(inputStr))
}

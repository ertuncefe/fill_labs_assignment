package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func FindTheMaxOccurence(input string) string {
	//Tokenize that input into a slice get rid of the quotes, split it with delimiter
	//"," and get rid of the braces
	filteredStr := strings.Replace(input, "\"", "", -1)
	tokenizedSlice := strings.Split(filteredStr[1:len(filteredStr)-1], ",")

	// Create an emty key value map
	wordCountMap := make(map[string]int)

	// In a loop count the words and initialize the key value map
	// if its already in map just add one to that word's value if not,
	// put the word inside the map with value 1
	for _, word := range tokenizedSlice {

		_, found := wordCountMap[word]

		if found {
			wordCountMap[word]++
		} else {
			wordCountMap[word] = 1
		}
	}

	//Loop again and select the word which occured maximum in the given input
	currentMax := 0
	var selectedWord string
	for word, wordOccurence := range wordCountMap {
		if currentMax < wordOccurence {
			currentMax = wordOccurence
			selectedWord = word
		}
	}

	return selectedWord
}

func main() {

	//Get the input from the console and convert it to the slice
	fmt.Println("Please enter an input as a string slice: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	inputStr := scanner.Text()

	//Start the calculating process of question03 and get the result
	selectedWord := FindTheMaxOccurence(inputStr)

	//Print the result
	fmt.Println("\"" + selectedWord + "\"")

}

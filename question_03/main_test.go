package main

import (
	"strings"
	"testing"
)

func TestTableFindTheMaxOccurence(t *testing.T) {
	var testSlice = []struct {
		input    string
		expected string
	}{
		//Test 1
		{"[\"apple\",\"pie\",\"apple\",\"red\",\"red\",\"red\"]", "\"red\""},
		//Test 2
		{"[\"apple\",\"pie\",\"apple\",\"red\",\"red\",\"red\",\"apple\",\"apple\",\"apple\",\"apple\"]", "\"apple\""},
		//Test 3
		{"\"test\",\"bottle\",\"bottle\",\"bottle\",\"building\",\"building\",\"building\",\"building\",\"building\",\"building\",\"building\"", "\"building\""},
	}

	for _, test := range testSlice {
		testResult := "\"" + FindTheMaxOccurence(test.input) + "\""
		if strings.Compare(testResult, test.expected) != 0 {
			t.Error("Test Failed! Output is " + testResult + " but expected " + test.expected)
		}
	}
}
